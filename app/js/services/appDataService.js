/**
 * # appDataService
 *
 * Initializes the application, gets the necessary enums from http:/json/enums file,
 * Builds the appDataService object
 *
 */

angular.module('app').service
	( 'appDataService',[
		'$q'
		, '$rootScope'
		, '$log'
		, '$http'
		, '$templateCache'
		, '$route','$timeout'
		, function(
			$q
			, $rootScope
			, $log
			, $http
			, $templateCache
			, $route
			,$timeout
			) {

			$rootScope.default = "JFK"
			$rootScope.process = false;

			var appDataService = {}
				,items = []
				,airport = $rootScope.default
				,allAirports = {}



			/**
			 * Get the necessary files and enums from server
			 *
			 * @returns {promise|a.fn.promise}
			 */
			this.initApplication = function(){

				console.log('WELCOME TO AMERICAN AIRLINE MENU')

				var defer = $q.defer()
				$http({method: "GET", url: 'http://americanairlines.mmdn.net/php/airports.php', cache: $templateCache} ).success(function(data, status) {
					if(status === 200){

						console.log('GATHERING THE LIST OF AIRPORTS AVAILABLE')

						allAirports = data
						getAirport().then(function(airport){
							$rootScope.process = true;
							console.log('AIRPORT >> ',airport)
							getMenuItems(airport ).then(function(data){
								$log.log('data >>>>>>>>>>>>>>>>>>>',data)
								getJsonConfigs(data.airport).then(function(data){
									defer.resolve(data);
								})
							})
						});
					}

				}).error(function(data, status) {
					$log.debug('failed getting airport list >',status,data);

				});
				return defer.promise;
			}

			this.getAllAirports = function(){
				return allAirports;
			}

			this.updateAirports = function(airport){
				var defer = $q.defer()
				console.log('updateAirports >> ',airport)
				getMenuItems(airport ).then(function(data){
					defer.resolve(data);
				})

				return defer.promise;
			}

			function getMenuItems(airport){
				var defer = $q.defer()

				$http({method: "GET", url: './json/'+airport+'/menu.json', cache: $templateCache} ).success(function(data, status) {
					if(status === 200){
						items = data.items
						data.airport = airport
						defer.resolve(data);
					}else{
						$log.debug('failed getting menu.json >',status,data);
					}

				}).error(function(data, status) {
					$log.debug('failed getting routeParams >',status,data);
					getMenuItems($rootScope.default).then(function(data){
						$log.debug('GOT '+$rootScope.default+' >',data);
						data.airport = $rootScope.default
						defer.resolve(data);
					})
				});

				return defer.promise;
			}


			function objectKeyExists(value, obj ) {
				// null and undefined are empty
				if( obj === null || typeof(obj) == 'undefined' ) {
					return false;
				}
				var results = false;
				Object.keys(obj).forEach(function(key) {

					if(key.toLowerCase() ===  value.trim().toLowerCase()){
						results = true;
					}

				});

				return results;
			};


			var getGeoLocation = function(callback) {

				if (typeof(navigator.geolocation) != 'undefined') {
					navigator.geolocation.getCurrentPosition(function(position) {
						$rootScope.process = true
						callback.call(this,position);
					}, function(err){
						callback.call(this,false);
					});
				}else{
					callback.call(this,false);
				}
			}



			function getAirport(){
				console.log('GETTING AIRPORT >> ')

				var defer = $q.defer()

				defer.resolve($rootScope.default);

//				if (typeof (navigator) !== "undefined" && "geolocation" in navigator) {
//
//					var location = getGeoLocation(function(position){
//
//						if(position){
//							$http({method: "GET", url: 'http://americanairlines.mmdn.net/php/?lat='+position.coords.latitude+'&long='+position.coords.longitude, cache: $templateCache} ).success(function(data, status) {
//								if(status === 200 && typeof (data.faa_code) != "undefined"){
//									var airport = typeof (data['faa_code']) !== "undefined"?data['faa_code']:(typeof ($route.current.params['airport']) !== "undefined"?$route.current.params['airport']:$rootScope.default);
//									$log.debug('airport >',airport);
//									defer.resolve(airport);
//								}else{
//									defer.resolve($rootScope.default);
//								}
//							}).error(function() {
//								defer.resolve($rootScope.default);
//							});
//						}else{
//							defer.resolve($rootScope.default);
//						}
//
//					});
//
//
//					$timeout(function(){
//						if(!$rootScope.process)
//							defer.resolve($rootScope.default);
//					},3000)
//
//				} else {
//					console.log('GETTING AIRPORT >> NO geolocation')
//					defer.resolve($rootScope.default);
//				}

				return defer.promise;
			}

			this.menuItems = function(){
				return items;
			}

			this.i18n = function (){
				return appDataService;
			}

			/**
			 * Gets the json file from $http.get(url) request, once success data is gathered it will update
			 * appDataService with new keys based on request url file name
			 * @param file
			 * @returns {exports.pending.promise|*|promise|defer.promise|promiseContainer.promise|a.fn.promise}
			 */
			function getJsonConfigs (airport){
				var deferred  = $q.defer(),url,key, handler = [];

				for(var i in items){
					if(String(items[i].file ).trim() !== ""){
						handler.push($http({method: "GET", url: './json/'+airport+'/'+items[i].file+'.json', cache: $templateCache}))
					}
				}
				$q.all( handler ).then(function(arrayOfResults){
					for(var i in arrayOfResults){
						if(arrayOfResults[i].status == 200){
							url =  arrayOfResults[i].config.url;
							key = url.substring( url.lastIndexOf("/") + 1 ,url.indexOf('.json'));
							appDataService[key] = arrayOfResults[i].data
						}
					}
					deferred.resolve();

				}, function(data){
					$log.debug('failed getting json >',data);
				});

				return deferred .promise;
			}


		}]
	)