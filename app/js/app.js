'use strict';
/**
 * # Main application declaration file
 *
 * Allows main application to be declared. This seperate file is required in
 * order to properly isolate angular logic from requirejs module loading
 */
angular.module(
		'app', [
			'ngRoute'
			, 'ngResource'
			, 'ngSanitize'
			, 'ngTouch'
		])
