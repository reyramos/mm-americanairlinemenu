'use strict';
// Require JS  Config File
require({
		baseUrl: '/js/',
		paths: {
			  'angular': '../lib/angular/angular'
			, 'angular-resource': '../lib/angular-resource/index'
			, 'angular-route': '../lib/angular-route/index'
			, 'angular-sanitize': '../lib/angular-sanitize/index'
			, 'angular-touch': '../lib/angular-touch/index'
			, 'jquery': '../lib/jquery/dist/jquery'
		},
		map: {
			'*': { 'jquery': 'jquery' }, 'noconflict': { "jquery": "jquery" }
		},
		shim: {
			'app': { 'deps': [
				'angular'
				, 'angular-route'
				, 'angular-resource'
				, 'angular-sanitize'
				, 'angular-touch'

			]}
			, 'config': { 'deps': ['app'] }
			, 'routes': { 'deps': ['app'] }
			, 'paths': { 'deps': ['app'] }
			, 'angular-route': { 'deps': ['angular', 'jquery'], exports: 'angular' }
			, 'angular-resource': { 'deps': ['angular'] }
			, 'angular-sanitize': { 'deps': ['angular'] }
			, 'angular-touch': { 'deps': ['angular'] }
			, 'jquery': {
				init: function ($) {
					return $.noConflict(true);
				},
				exports: 'jquery'
			}
			, 'controllers/ApplicationController': {
				'deps': [
					'app'
				]}
			, 'services/appDataService': {
				'deps': [
					'app'
				]}
		}}
	, [
		'require'
	    , 'paths'
	    , 'routes'
	    , 'config'
		, 'controllers/ApplicationController'
		, 'services/appDataService'
	]
	, function (require) {
		return require(
			[
				'bootstrap'
			]
		)
	}
);