/**
 * # Static File Paths
 *
 * Inject URL paths for fonts/images and potentially other static files
 * This file exists due to the need of dynamically generating absolute
 * paths to all static files for versioning reasons.
 *
 */

angular.module('app').run(
	['$rootScope' , '$log' , '$routeParams'
		, function( $rootScope , $log , $routeParams ) {
		$rootScope.$on
		( '$routeChangeSuccess'
			, function() {
				var s = document.styleSheets[document.styleSheets.length - 1];

				/**
				 * This is specifically to handle a case where Firefox/Netscape does not have
				 * the addRule() function and fails.
				 */
				function addCSSRule(sheet, selector, rules, index) {
					if( sheet.insertRule ) {
						sheet.insertRule( selector + "{" + rules + "}", index );
					} else {
						sheet.addRule(selector, rules, index);
					}
				}

				addCSSRule
				(	s
					,	'@font-face'
					,	[
						  "font-family: 'Agenda-Black';"
						, "src: url('/fonts/agenda-black.eot?#iefix') format('embedded-opentype'),"
						, "url('/fonts/agenda-black.woff') format('woff'),"
						, "url('/fonts/agenda-black.ttf')  format('truetype'),"
						, "url('/fonts/agenda-black.svg#Agenda-Black') format('svg');"
						, "font-weight: normal;"
						, "font-style: normal;"
				].join('\n'));

				addCSSRule
				(	s
					,	'@font-face'
					,	[
					"font-family: 'Agenda-Light';"
					, "src: url('/fonts/agenda-light.eot?#iefix') format('embedded-opentype'),"
					, "url('/fonts/agenda-light.woff') format('woff'),"
					, "url('/fonts/agenda-light.ttf')  format('truetype'),"
					, "url('/fonts/agenda-light.svg#Agenda-Light') format('svg');"
					, "font-weight: normal;"
					, "font-style: normal;"
				].join('\n'));
				addCSSRule
				(	s
					,	'@font-face'
					,	[
					"font-family: 'Agenda-Bold';"
					, "src: url('/fonts/agenda-bold.eot?#iefix') format('embedded-opentype'),"
					, "url('/fonts/agenda-bold.woff') format('woff'),"
					, "url('/fonts/agenda-bold.ttf')  format('truetype'),"
					, "url('/fonts/agenda-bold.svg#Agenda-Bold') format('svg');"
					, "font-weight: normal;"
					, "font-style: normal;"
				].join('\n'));
				addCSSRule
				(	s
					,	'@font-face'
					,	[
					"font-family: 'Agenda-Medium';"
					, "src: url('/fonts/agenda-medium.eot?#iefix') format('embedded-opentype'),"
					, "url('/fonts/agenda-medium.woff') format('woff'),"
					, "url('/fonts/agenda-medium.ttf')  format('truetype'),"
					, "url('/fonts/agenda-medium.svg#Agenda-Medium') format('svg');"
					, "font-weight: normal;"
					, "font-style: normal;"
				].join('\n'));
				addCSSRule
				(	s
					,	'@font-face'
					,	[
					"font-family: 'Agenda-Regular';"
					, "src: url('/fonts/agenda-regular.eot?#iefix') format('embedded-opentype'),"
					, "url('/fonts/agenda-regular.woff') format('woff'),"
					, "url('/fonts/agenda-regular.ttf')  format('truetype'),"
					, "url('/fonts/agenda-regular.svg#Agenda-Regular') format('svg');"
					, "font-weight: normal;"
					, "font-style: normal;"
				].join('\n'));
				addCSSRule
				(	s
					,	'@font-face'
					,	[
					"font-family: 'Agenda-Semibold';"
					, "src: url('/fonts/agenda-semibold.eot?#iefix') format('embedded-opentype'),"
					, "url('/fonts/agenda-semibold.woff') format('woff'),"
					, "url('/fonts/agenda-semibold.ttf')  format('truetype'),"
					, "url('/fonts/agenda-semibold.svg#Agenda-Semibold') format('svg');"
					, "font-weight: normal;"
					, "font-style: normal;"
				].join('\n'));
				addCSSRule
				(	s
					,	'@font-face'
					,	[
					"font-family: 'Agenda-Thin';"
					, "src: url('/fonts/agenda-thin.eot?#iefix') format('embedded-opentype'),"
					, "url('/fonts/agenda-thin.woff') format('woff'),"
					, "url('/fonts/agenda-thin.ttf')  format('truetype'),"
					, "url('/fonts/agenda-thin.svg#Agenda-Thin') format('svg');"
					, "font-weight: normal;"
					, "font-style: normal;"
				].join('\n'));
			}
		)
	}]
)