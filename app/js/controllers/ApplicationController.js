'use strict';
/**
 * ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application, no application logic done at the upper level
 *
 */
angular.module('app').controller(
	'ApplicationController',
	[
		'$scope'
		,'$q'
		, '$rootScope'
		, '$log'
		, 'appDataService'
		, '$timeout','$http','$templateCache'
		, function
			($scope
			 ,$q
				, $rootScope
				, $log
				, appDataService
			 	, $timeout
			 ,$http,$templateCache
				) {


		$scope.i18n = appDataService.i18n();
		$scope.menuItems = appDataService.menuItems();



		$scope.allAirports = appDataService.getAllAirports();
		console.log('$scope.allAirports',$scope.allAirports);
		console.log('$scope.menuItems',$scope.menuItems);


		var appDataService;


		$scope.modal = {}
		$scope.backdrop = { active: false};
		$scope.selectAirport = { active: false};

		var position = 0;
		var start = true;
		var setView = ''

		function safeApply(scope, fn) {
			(scope.$$phase || scope.$root.$$phase) ? fn() : scope.$apply(fn);
		}

		var selectAirport = function (){
			safeApply($scope,function(){
				$scope.backdrop.active = true;
				$scope.selectAirport.active = true;
			})
		}()


		$scope.updateAirport = function(airport){

			if(typeof (airport) === "undefined") return;

			appDataService.updateAirports(airport ).then(function(data){
				safeApply($scope,function(){
					$scope.menuItems = appDataService.menuItems();
				})

				getJsonConfigs(data.airport, data.items ).then(function(data){
					safeApply($scope,function(){
						$scope.i18n = appDataService;
						$scope.backdrop.active = false;
						$scope.selectAirport.active = false;
					})
				})
			});

		}


		function getJsonConfigs (airport, items){
			var deferred  = $q.defer(),url,key, handler = [];

			for(var i in items){
				if(String(items[i].file ).trim() !== ""){
					handler.push($http({method: "GET", url: './json/'+airport+'/'+items[i].file+'.json', cache: $templateCache}))
				}
			}
			$q.all( handler ).then(function(arrayOfResults){
				for(var i in arrayOfResults){
					if(arrayOfResults[i].status == 200){
						url =  arrayOfResults[i].config.url;
						key = url.substring( url.lastIndexOf("/") + 1 ,url.indexOf('.json'));
						appDataService[key] = arrayOfResults[i].data
					}
				}
				deferred.resolve();

			}, function(data){
				$log.debug('failed getting json >',data);
			});

			return deferred .promise;
		}


		$scope.setModal = function(view){

			if(typeof (view) === "undefined" || view === "")return;

			position = 0;
			start = true;
			setView = view

			angular.element(document.getElementById(view) ).addClass('selected');
			$scope.nextButton = $('#'+setView).next()[0].id?true:false;

			var strings = $scope.i18n[view];
			$scope.modal.active = true;
			$scope.backdrop.active = true;
			$scope.modal.header = strings.header;
			$scope.modal.subHeading = strings.subHeading;
			$scope.modal.packages = strings.packages;
			$scope.modal.footer = strings.footer;
			$timeout(function(){
				var height = $('#scroll').height();
				$scope.modal.arrow = height > 430?true:false;
			},100)
		}

		$scope.scrollDown = function(){
			var height = $('#scroll').height()
			//get the scroll position
			position = $('.modal-body' ).scrollTop();


			position += 400;
			if(position > height) start = false;
			if(start){
				$('.modal-body').animate({scrollTop:position}, 500);
			}

		}

		$scope.back = function(){
			var prev = $('#'+setView).prev()
			if(prev.length){
				prev = prev[0].id
				//remove the previous class
				angular.element(document.getElementById(setView) ).removeClass('selected')
				$scope.setModal(prev)
			}else{
				angular.element(document.getElementById(setView) ).removeClass('selected')
				setView = ''

				$('.modal-body' ).scrollTop(0)
				$timeout(function(){
					$scope.modal = {}
				},100)
			}

		}

		$scope.removeModal = function(){
			angular.element(document.getElementById(setView) ).removeClass('selected')
			setView = ''

			$('.modal-body' ).scrollTop(0)
			$timeout(function(){
				$scope.modal = {}
				$scope.backdrop.active = false;

			},100)
		}

		$scope.next = function(){
			var next = $('#'+setView).next()[0].id
			if(next){
				//remove the previous class
				angular.element(document.getElementById(setView) ).removeClass('selected')
				$scope.setModal(next)
				$('.modal-body' ).scrollTop(0)
			}
		}

	}]
)
