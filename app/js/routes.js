'use strict';

angular.module('app').config(
	[
		  '$routeProvider'
		, '$locationProvider'
		, '$logProvider'
		, 'enums'
	, function ($routeProvider, $locationProvider, $logProvider, enums) {
		/**
		 * Initialize the application first before loading any controllers
		 *
		 * @type {*[]}
		 */

		var initializeData =
			[ 'appDataService'
				, function
				  ( appDataService ){

				return appDataService.initApplication();

			}];


		$routeProvider
			.when('/',
			{
				  templateUrl: '/views/index.html'
				, controller: 'ApplicationController'
				, resolve: { 'initializeData': initializeData }
			})
			.when('/menu',
				  {
					  templateUrl: '/views/index.html'
					  , controller: 'ApplicationController'
					  , resolve: { 'initializeData': initializeData }
			})
			.when('/feedback',
			{
				  templateUrl: '/views/feedback.html'
			})




		var debug = getBool(location.search.split('debug=')[1]) || false
		function getBool(string){
			var string = String(string).toLocaleLowerCase()
			switch (string){
				case 'true':
				case '1':
					return true
				break;
				default:
					return false;
				break;
			}
		}

		/**
		 * Enable logging debugging
		 */
		$logProvider.debugEnabled(debug?debug:enums.debug || false);

		/**
		 * ## HTML5 pushState support
		 *
		 * This enables urls to be routed with HTML5 pushState so they appear in a
		 * '/someurl' format without a page refresh
		 *
		 * The server must support routing all urls to index.html as a catch-all for
		 * this to function properly,
		 *
		 * The alternative is to disable this which reverts to '#!/someurl'
		 * anchor-style urls.
		 */
		if(window.history && window.history.pushState){
			$locationProvider.html5Mode(true);
		}

	}])