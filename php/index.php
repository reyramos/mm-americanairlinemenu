<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 9/3/14
 * Time: 10:19 AM
 */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');
header('content-type: application/json; charset=utf-8');

error_reporting(E_ALL);
ini_set("display_errors", 1);

$host = "10.190.25.235";
$user = "creative";
$pass = "ch3vych453";
$name = "americanairlines_mmdn_net";
$handler = array();


if (isset($_GET['lat']) && $_GET['lat'] != "" && isset($_GET['long']) && $_GET['long'] != ""):
	$lat = floatval($_GET['lat']);
	$long = floatval($_GET['long']);


//fudge = Math.pow(Math.cos(Math.toRadians(lat)), 2);
	$fudge = pow(cos(($lat * M_PI / 180)), 2);;
	$query = "SELECT * FROM airports WHERE is_aa = 1 AND faa_code IS NOT NULL ORDER BY (($lat - lat) * ($lat - lat) + ($long - 'long') * ($long - 'long') * $fudge ) LIMIT 1";

	try {
		$dbh = new PDO("mysql:host=$host;dbname=$name", $user, $pass);
		foreach ($dbh->query($query) as $row) {
			$handler = array_merge($handler, $row);
		}
		$dbh = null;
	} catch (PDOException $e) {
		print "Error!: " . $e->getMessage() . "<br/>";
		die();
	}


endif;


if (isset($_GET['code']) && $_GET['code'] != ""):
	$query = "SELECT * FROM airports WHERE faa_code='".strtoupper($_GET['code'])."'";
	try {
		$dbh = new PDO("mysql:host=$host;dbname=$name", $user, $pass);
		$handler = array();
		foreach ($dbh->query($query) as $row) {
			$handler = array_merge($handler, $row);
		}
		$dbh = null;
	} catch (PDOException $e) {
		print "Error!: " . $e->getMessage() . "<br/>";
		die();
	}

endif;


echo json_encode($handler);
