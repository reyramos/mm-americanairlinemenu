<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 9/3/14
 * Time: 10:19 AM
 */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');
header('content-type: application/json; charset=utf-8');

error_reporting(E_ALL);
ini_set("display_errors", 1);

$host = "10.190.25.235";
//$host = "localhost";
$user = "creative";
$pass = "ch3vych453";
$name = "americanairlines_mmdn_net";

$directory = dirname(dirname(__FILE__)) . "/json/";
$file = scandir($directory);
$handler = array();

try {
	$dbh = new PDO("mysql:host=$host;dbname=$name", $user, $pass);
	$sth = $dbh->prepare("SELECT * FROM airports WHERE faa_code = :file");

	foreach ($file as $file) {
		if ($file != ".." && $file != ".") {
			$sth->execute(array(':file' => $file));
			$result = $sth->fetchAll();
			$handler[$result[0]['name']] = $result[0];
		}
	}


	$dbh = null;
} catch (PDOException $e) {
	print "Error!: " . $e->getMessage() . "<br/>";
	die();
}

ksort($handler);

$airpports = array();
foreach($handler as $airport){
	$airpports[] = $airport;
}

echo json_encode($airpports);
